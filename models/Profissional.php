<?php

class Profissional extends Model
{
    public function getAllProfissionais(){

        $query = "SELECT pe.nome,
                         pe.id,
                         pe.rg,
                         pe.data_nasc,
                         pe.cpf,
                         pe.telefoneprincipal,
                         pe.telefonesecundario,
                         en.rua,
                         en.cep,
                         en.numero,
                         pr.tipo
                            FROM pessoa pe
                              JOIN endereco en ON pe.endereco_id = en.id
                                JOIN profissional pr ON pe.id = pr.pessoa_id 
                                  WHERE pe.ativo = 1  ORDER BY pe.nome
        ";

        $profissionais = $this->db->prepare($query);
        $profissionais->execute();

        return $profissionais->fetchAll();

    }

    public function getProfissionalById($id){

        $query = "SELECT pe.nome,
                         pe.id as id_pessoa,
                         pe.rg,
                         pe.data_nasc,
                         pe.cpf,
                         pe.sexo_id,
                         pe.telefoneprincipal,
                         pe.telefonesecundario,
                         en.id as id_endereco,
                         en.rua,
                         en.cep,
                         en.numero,
                         pr.id as id_profissional,
                         pr.tipo,
                         se.id,
                         se.descricao
                            FROM pessoa pe
                              JOIN endereco en ON pe.endereco_id = en.id
                                JOIN profissional pr ON pe.id = pr.pessoa_id 
                                  JOIN sexo se ON pe.sexo_id = se.id 
                                  WHERE pe.id = $id and pe.ativo = 1
        ";

        $profissionais = $this->db->prepare($query);

        $profissionais->execute();

        return $profissionais->fetch(PDO::FETCH_ASSOC);
    }

    public function getAllSexos(){

        $query = "SELECT * FROM sexo";
        $sexos = $this->db->prepare($query);
        $sexos->execute();

        return $sexos->fetchAll();

    }

    public function insertEndereco($endereco){

        $sql = $this->db->prepare("INSERT INTO endereco (rua, numero, cep) VALUES (:rua, :numero, :cep) RETURNING id");


        $sql->bindValue(':rua',    $endereco['rua']);
        $sql->bindValue(':numero', $endereco['numero']);
        $sql->bindValue(':cep',    $endereco['cep']);
        $sql->execute();
        $enderecoId = $sql->fetch(PDO::FETCH_ASSOC);

        $this->insertPessoa($endereco, $enderecoId);
    }

    public function insertPessoa($pessoa, $enderecoId){

        $sql = $this->db->prepare("INSERT INTO  pessoa (nome, data_nasc, rg, cpf, sexo_id, endereco_id, telefoneprincipal, telefonesecundario, ativo) 
                                          VALUES (:nome, :data_nasc, :rg, :cpf, :sexo_id, :endereco_id, :telefoneprincipal, :telefonesecundario, 1) RETURNING id");
        $sql->bindValue(':nome',      $pessoa['nome']);
        $sql->bindValue(':data_nasc', $pessoa['data_nasc']);
        $sql->bindValue(':rg',        $pessoa['rg']);
        $sql->bindValue(':cpf',       $pessoa['cpf']);
        $sql->bindValue(':sexo_id',   $pessoa['sexo_id']);
        $sql->bindValue(':endereco_id',$enderecoId['id']);
        $sql->bindValue(':telefoneprincipal', $pessoa['telefoneprincipal']);
        $sql->bindValue(':telefonesecundario',$pessoa['telefonesecundario']);
        $sql->execute();

        $pessoaId = $sql->fetch(PDO::FETCH_ASSOC);
        $this->insertProfissional($pessoa, $pessoaId);
    }

    public function insertProfissional($profissional, $pessoaId){

        $sql = $this->db->prepare("INSERT INTO  profissional (tipo, created_at, pessoa_id) VALUES (:tipo,now(),:pessoa_id)");

        $sql->bindValue(':tipo', $profissional['tipo']);
        $sql->bindValue(':pessoa_id', $pessoaId['id']);
        $sql->execute();


    }

    public function updateEndereco($endereco){

        $sql = $this->db->prepare("UPDATE endereco SET rua = :rua, numero = :numero, cep = :cep WHERE id = :id");


        $sql->bindValue(':rua',    $endereco['rua']);
        $sql->bindValue(':numero', $endereco['numero']);
        $sql->bindValue(':cep',    $endereco['cep']);
        $sql->bindValue(':id',     $endereco['id_endereco']);
        $sql->execute();

        $this->updatePessoa($endereco);

    }

    public function updatePessoa($pessoa){

        $sql = $this->db->prepare("UPDATE pessoa SET nome = :nome, data_nasc = :data_nasc, rg = :rg, cpf = :cpf, sexo_id = :sexo_id, endereco_id = :endereco_id, 
                                telefoneprincipal = :telefoneprincipal, telefonesecundario = :telefonesecundario WHERE id = :id");
        $sql->bindValue(':nome',       $pessoa['nome']);
        $sql->bindValue(':data_nasc',  $pessoa['data_nasc']);
        $sql->bindValue(':rg',         $pessoa['rg']);
        $sql->bindValue(':cpf',        $pessoa['cpf']);
        $sql->bindValue(':sexo_id',    $pessoa['sexo_id']);
        $sql->bindValue(':endereco_id',$pessoa['id_endereco']);
        $sql->bindValue(':telefoneprincipal', $pessoa['telefoneprincipal']);
        $sql->bindValue(':telefonesecundario',$pessoa['telefonesecundario']);
        $sql->bindValue(':id',$pessoa['id_pessoa']);
        $sql->execute();

        $this->updateProfissional($pessoa);

    }

    public function updateProfissional($profissional){

        $sql = $this->db->prepare("UPDATE profissional SET tipo = :tipo WHERE id = :id");

        $sql->bindValue(':tipo',      $profissional['tipo']);
        $sql->bindValue(':id',             $profissional['id_profissional']);
        $sql->execute();
    }

    public function deleteProfissional($id){

        $sql = $this->db->prepare("UPDATE pessoa SET ativo = :ativo WHERE id = :id");

        $sql->bindValue(':id', $id);
        $sql->bindValue(':ativo', 0);
        $sql->execute();
    }

}