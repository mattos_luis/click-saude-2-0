<?php

class Auth extends Model
{
    public function doLogin($user){

        $sql = $this->db->prepare("SELECT us.username,us.senha, pe.nome, per.nivel_perfil FROM pessoa pe JOIN usuario us ON us.pessoa_id = pe.id JOIN perfil_usuario ON
  perfil_usuario.usuario_id = us.id JOIN perfil per ON per.id = perfil_usuario.perfil_id WHERE us.username = :username AND us.senha = :senha");
        $sql->bindValue(':username', $user['username']);
        $sql->bindValue(':senha',md5($user['senha']));
        $sql->execute();

        if($sql->rowCount() > 0){

           $usuario = $sql->fetch();

           $_SESSION['username'] =  $usuario['username'];
           $_SESSION['id']       =  $usuario['id'];
           $_SESSION['perfil']   =  $usuario['nivel_perfil'];
           $_SESSION['isLogged'] =  true;

        }else{
           $_SESSION['isLogged'] =  false;
           $dados['error-message'] = "Usuário ou senha incorretos";

           return $dados;
        }
    }

}