<?php

class Consulta extends Model
{
    public function getAll($filtro){



        $query = "SELECT pe.nome,
                         co.data_consulta,
                         co.horario,
                         co.anotacao,
                         co.realizada,
                         co.id
                            FROM pessoa pe
                              INNER JOIN paciente pa ON pe.id = pa.pessoa_id
                              INNER JOIN consulta co ON pa.id = co.paciente_id 
                                WHERE co.realizada = :realizada;
        ";

        $consultas = $this->db->prepare($query);
        $consultas->bindValue(':realizada', $filtro['filtro_realizada']);

        $consultas->execute();

        return $consultas->fetchAll(PDO::FETCH_ASSOC);
    }

    public function marcarConsulta($consulta){

        $sql = $this->db->prepare("INSERT INTO consulta (data_consulta, horario, paciente_id, profissional_id, realizada) VALUES (:data_consulta, :horario, :paciente_id, :profissional_id, :realizada)");
        $sql->bindValue(':data_consulta',  $consulta['data']);
        $sql->bindValue(':horario',        $consulta['hora']);
        $sql->bindValue(':paciente_id',    $consulta['paciente']);
        $sql->bindValue(':profissional_id',$consulta['profissional']);
        $sql->bindValue(':realizada',      0);

        $sql->execute();


    }

    public function getPacientes(){

        $query = "SELECT pe.nome,
                         pa.id
                         FROM pessoa pe 
                           JOIN paciente pa ON pe.id = pa.pessoa_id";

        $sql = $this->db->prepare($query);
        $sql->execute();

        return $sql->fetchAll();

    }

    public function realizarConsulta($consulta){

        $consulta['foto'] = $consulta['foto'] ? $consulta['foto'] : null;
        $consulta['video'] = $consulta['video'] ? $consulta['video'] : null;
        $consulta['id_consulta'] = intval($consulta['id_consulta']);

        $sql = $this->db->prepare("UPDATE consulta SET anotacao = :anotacao, foto = :foto, video = :video, realizada = :realizada WHERE id = :id");
        $sql->bindValue(':anotacao', $consulta['anotacao']);
        $sql->bindValue(':foto',     $consulta['foto']);
        $sql->bindValue(':video',    $consulta['video']);
        $sql->bindValue(':realizada',    1);
        $sql->bindValue(':id',       $consulta['id_consulta']);
        $sql->execute();

    }

    public function getConsultaById($id){

        $sql= $this->db->prepare("SELECT id, data_consulta, horario FROM consulta WHERE id = :id");
        $sql->bindValue(':id', $id);
        $sql->execute();

        return $sql->fetch();

    }

    public function reagendarConsulta($consulta){

        $sql = $this->db->prepare("UPDATE consulta SET data_consulta = :data_consulta, horario = :horario WHERE id = :id");
        $sql->bindValue(':data_consulta',  $consulta['data_consulta']);
        $sql->bindValue(':horario',        $consulta['horario']);
        $sql->bindValue(':id',             $consulta['id']);
        $sql->execute();


    }

}