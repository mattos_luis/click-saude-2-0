<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 12/16/2017
 * Time: 11:50 PM
 */

class Relatorio extends Model
{

    public function pequisaSexo(){

        $query = "SELECT s.descricao, 
                         count(p.id) as qtd
                      FROM pessoa as p
                        JOIN sexo as s ON p.sexo_id = s.id
                        GROUP BY s.descricao;";

        $pessoas = $this->db->prepare($query);
        $pessoas->execute();

        return $pessoas->fetchAll(PDO::FETCH_ASSOC);
    }
}