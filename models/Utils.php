<?php

class Utils extends Model
{
    public function getCountProfissionais(){

        $query = "SELECT count(pr.id) as totProfissionais
                            FROM pessoa pe
                                JOIN profissional pr ON pe.id = pr.pessoa_id 
                                  WHERE created_at > CURRENT_DATE - 1;
        ";

        $profissionais = $this->db->prepare($query);
        $profissionais->execute();

        return $profissionais->fetch();
    }

    public function getCountPacientes(){

        $query = "SELECT count(pa.id) as totPacientes
                            FROM pessoa pe
                                JOIN paciente pa ON pe.id = pa.pessoa_id 
                                  WHERE created_at > CURRENT_DATE - 1";

        $pacientes = $this->db->prepare($query);
        $pacientes->execute();

        return $pacientes->fetch();
    }

    public function getCountConsultas(){
        $query = "SELECT COUNT(id) as totConsultas FROM consulta WHERE realized_at > CURRENT_DATE -1";

        $consulta = $this->db->prepare($query);
        $consulta->execute();

        return $consulta->fetch();
    }
}