<?php

class Paciente extends Model
{
    public function getAllPacientes(){

        $query = "SELECT pe.nome,
                         pe.id,
                         pe.rg,
                         pe.data_nasc,
                         pe.cpf,
                         pe.telefoneprincipal,
                         pe.telefonesecundario,
                         en.rua,
                         en.cep,
                         en.numero,
                         pa.convenios,
                         pa.paciente_antigo_especial,
                         pa.tipo_indicacao,
                         pa.altura,
                         pa.peso
                            FROM pessoa pe
                              JOIN endereco en ON pe.endereco_id = en.id
                                JOIN paciente pa ON pe.id = pa.pessoa_id 
                                  WHERE pe.ativo = 1 ORDER BY pe.nome 
        ";

        $pacientes = $this->db->prepare($query);
        $pacientes->execute();

        return $pacientes->fetchAll();

    }

    public function getPacienteById($id){

        $query = "SELECT pe.nome,
                         pe.id as id_pessoa,
                         pe.rg,
                         pe.data_nasc,
                         pe.cpf,
                         pe.sexo_id,
                         pe.telefoneprincipal,
                         pe.telefonesecundario,
                         en.id as id_endereco,
                         en.rua,
                         en.cep,
                         en.numero,
                         pa.id as id_paciente,
                         pa.convenios,
                         pa.paciente_antigo_especial,
                         pa.tipo_indicacao,
                         pa.altura,
                         pa.peso,
                         se.id,
                         se.descricao
                            FROM pessoa pe
                              JOIN endereco en ON pe.endereco_id = en.id
                                JOIN paciente pa ON pe.id = pa.pessoa_id 
                                  JOIN sexo se ON pe.sexo_id = se.id 
                                  WHERE pe.id = $id  AND ativo = 1;
        ";

        $pacientes = $this->db->prepare($query);

        $pacientes->execute();

        return $pacientes->fetch(PDO::FETCH_ASSOC);
    }

    public function getAllSexos(){

        $query = "SELECT * FROM sexo";
        $sexos = $this->db->prepare($query);
        $sexos->execute();

        return $sexos->fetchAll();

    }

    public function insertEndereco($endereco){

        $sql = $this->db->prepare("INSERT INTO endereco (rua, numero, cep) VALUES (:rua, :numero, :cep) RETURNING id");


        $sql->bindValue(':rua',    $endereco['rua']);
        $sql->bindValue(':numero', $endereco['numero']);
        $sql->bindValue(':cep',    $endereco['cep']);
        $sql->execute();
        $enderecoId = $sql->fetch(PDO::FETCH_ASSOC);

        $this->insertPessoa($endereco, $enderecoId);
    }

    public function insertPessoa($pessoa, $enderecoId){

        $sql = $this->db->prepare("INSERT INTO  pessoa (nome, data_nasc, rg, cpf, sexo_id, endereco_id, telefoneprincipal, telefonesecundario, ativo) 
                                          VALUES (:nome, :data_nasc, :rg, :cpf, :sexo_id, :endereco_id, :telefoneprincipal, :telefonesecundario, 1) RETURNING id");
        $sql->bindValue(':nome',      $pessoa['nome']);
        $sql->bindValue(':data_nasc', $pessoa['data_nasc']);
        $sql->bindValue(':rg',        $pessoa['rg']);
        $sql->bindValue(':cpf',       $pessoa['cpf']);
        $sql->bindValue(':sexo_id',   $pessoa['sexo_id']);
        $sql->bindValue(':endereco_id',$enderecoId['id']);
        $sql->bindValue(':telefoneprincipal', $pessoa['telefoneprincipal']);
        $sql->bindValue(':telefonesecundario',$pessoa['telefonesecundario']);
        $sql->execute();

        $pessoaId = $sql->fetch(PDO::FETCH_ASSOC);
        $this->insertPaciente($pessoa, $pessoaId);
    }

    public function insertPaciente($paciente, $pessoaId){

        $sql = $this->db->prepare("INSERT INTO  paciente (convenios, paciente_antigo_especial, tipo_indicacao,
            altura, peso, created_at, pessoa_id) VALUES (:convenios, :paciente_antigo_especial, :tipo_indicacao, :altura, :peso, now(), :pessoa_id)");

        $sql->bindValue(':convenios', $paciente['convenios']);
        $sql->bindValue(':paciente_antigo_especial', $paciente['paciente_antigo_especial']);
        $sql->bindValue(':tipo_indicacao', $paciente['tipo_indicacao']);
        $sql->bindValue(':altura',    $paciente['altura']);
        $sql->bindValue(':peso',      $paciente['peso']);
        $sql->bindValue(':pessoa_id', $pessoaId['id']);
        $sql->execute();


    }

    public function updateEndereco($endereco){

        $sql = $this->db->prepare("UPDATE endereco SET rua = :rua, numero = :numero, cep = :cep WHERE id = :id");


        $sql->bindValue(':rua',    $endereco['rua']);
        $sql->bindValue(':numero', $endereco['numero']);
        $sql->bindValue(':cep',    $endereco['cep']);
        $sql->bindValue(':id',     $endereco['id_endereco']);
        $sql->execute();

        $this->updatePessoa($endereco);

    }

    public function updatePessoa($pessoa){

        $sql = $this->db->prepare("UPDATE pessoa SET nome = :nome, data_nasc = :data_nasc, rg = :rg, cpf = :cpf, sexo_id = :sexo_id, endereco_id = :endereco_id, 
                                telefoneprincipal = :telefoneprincipal, telefonesecundario = :telefonesecundario WHERE id = :id");
        $sql->bindValue(':nome',       $pessoa['nome']);
        $sql->bindValue(':data_nasc',  $pessoa['data_nasc']);
        $sql->bindValue(':rg',         $pessoa['rg']);
        $sql->bindValue(':cpf',        $pessoa['cpf']);
        $sql->bindValue(':sexo_id',    $pessoa['sexo_id']);
        $sql->bindValue(':endereco_id',$pessoa['id_endereco']);
        $sql->bindValue(':telefoneprincipal', $pessoa['telefoneprincipal']);
        $sql->bindValue(':telefonesecundario',$pessoa['telefonesecundario']);
        $sql->bindValue(':id',$pessoa['id_pessoa']);
        $sql->execute();

        $this->updatePaciente($pessoa);

    }

    public function updatePaciente($paciente){

        $sql = $this->db->prepare("UPDATE paciente SET convenios = :convenios, paciente_antigo_especial =  :paciente_antigo_especial, tipo_indicacao = :tipo_indicacao,
            altura = :altura, peso = :peso WHERE id = :id");

        $sql->bindValue(':convenios',      $paciente['convenios']);
        $sql->bindValue(':paciente_antigo_especial', $paciente['paciente_antigo_especial']);
        $sql->bindValue(':tipo_indicacao', $paciente['tipo_indicacao']);
        $sql->bindValue(':altura',         $paciente['altura']);
        $sql->bindValue(':peso',           $paciente['peso']);
        $sql->bindValue(':id',             $paciente['id_paciente']);
        $sql->execute();
    }

    public function deletePaciente($id){

        $sql = $this->db->prepare("UPDATE pessoa SET ativo = :ativo WHERE id = :id");

        $sql->bindValue(':id', $id);
        $sql->bindValue(':ativo', 0);
        $sql->execute();

    }

    public function anamneseExists($id){

        $sql = $this->db->prepare("SELECT a.atividade_fisica,
                                                   a.id,
                                                   a.medicamentos_em_uso,
                                                   a.queixas,
                                                   a.fraturas,
                                                   a.cirurgia
                                                    FROM anamnese a JOIN paciente pa ON a.id = pa.anamnese_id
                                                                      JOIN pessoa pe ON pa.pessoa_id = pe.id WHERE pe.id = :id");
        $sql->bindValue(':id', $id);
        $sql->execute();

        return $sql->fetch();


    }

    public function createAnamnese($anamnese){

        $sql = $this->db->prepare("INSERT INTO  anamnese (atividade_fisica, medicamentos_em_uso, queixas,
            fraturas, cirurgia) VALUES (:atividade_fisica,   :medicamentos_em_uso,:queixas, :fraturas, :cirurgia) RETURNING id");

        $sql->bindValue(':atividade_fisica',    $anamnese['atividade']);
        $sql->bindValue(':medicamentos_em_uso', $anamnese['medicamento']);
        $sql->bindValue(':fraturas', $anamnese['fratura']);
        $sql->bindValue(':queixas',  $anamnese['queixa']);
        $sql->bindValue(':cirurgia', $anamnese['cirurgia']);
        $sql->execute();

        $id = $sql->fetch(PDO::FETCH_ASSOC);

        $sql2 = $this->db->prepare("UPDATE paciente SET anamnese_id = :id WHERE pessoa_id = :id_paciente");
        $sql2->bindValue(':id', $id['id']);
        $sql2->bindValue(':id_paciente', intval($anamnese['paciente_id']));
        $sql2->execute();

    }

    public function updateAnamnese($anamnese){

        $sql = $this->db->prepare("UPDATE anamnese SET atividade_fisica    = :atividade_fisica, 
                                                                medicamentos_em_uso = :medicamentos_em_uso, 
                                                                queixas  = :queixas,
                                                                fraturas = :fraturas, 
                                                                cirurgia = :cirurgia  WHERE id = :id");

        $sql->bindValue(':atividade_fisica',    $anamnese['atividade']);
        $sql->bindValue(':medicamentos_em_uso', $anamnese['medicamento']);
        $sql->bindValue(':fraturas', $anamnese['fratura']);
        $sql->bindValue(':queixas',  $anamnese['queixa']);
        $sql->bindValue(':cirurgia', $anamnese['cirurgia']);
        $sql->bindValue(':id',       $anamnese['anamnese_id']);
        $sql->execute();

    }

}