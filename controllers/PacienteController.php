<?php


class PacienteController extends Controller
{

    public function __construct()
    {
        $this->canAccess();
    }

    public function index(){

       $this->getAll();

    }

    public function getAll(){

        $paciente = new Paciente();
        $dados['pacientes'] = $paciente->getAllPacientes();

        $this->loadTemplate('paciente/paciente',$dados);

    }

    public function get($id){

        //Chamar model método getPacienteById;

    }

    public function edit($id){

        $paciente = new Paciente();
        $dados['paciente'] = $paciente->getPacienteById($id);
        $dados['sexos'] = $paciente->getAllSexos();

        $this->loadTemplate('paciente/alterar', $dados);

    }

    public function update(){

        $paciente['rua']       = $_POST['rua'];
        $paciente['peso']      = $_POST['peso'];
        $paciente['numero']    = $_POST['numero'];
        $paciente['cep']       = $_POST['cep'];
        $paciente['nome']      = $_POST['nome'];
        $paciente['data_nasc'] = $_POST['data_nasc'];
        $paciente['rg']        = $_POST['rg'];
        $paciente['cpf']       = $_POST['cpf'];
        $paciente['sexo_id']   = $_POST['sexo_id'];
        $paciente['telefoneprincipal']  = $_POST['telefoneprincipal'];
        $paciente['telefonesecundario'] = $_POST['telefonesecundario'];
        $paciente['convenios']          = $_POST['convenios'];
        $paciente['paciente_antigo_especial'] = $_POST['paciente_antigo_especial'];
        $paciente['tipo_indicacao']           = $_POST['tipo_indicacao'];
        $paciente['altura']                   = $_POST['altura'];
        $paciente['id_pessoa']                   = $_POST['id_pessoa'];
        $paciente['id_paciente']                   = $_POST['id_paciente'];
        $paciente['id_endereco']                   = $_POST['id_endereco'];

        $pa = new Paciente();
        $pa->updateEndereco($paciente);

        header("Location:".BASE_URL."paciente");


    }

    public function delete($id){

        $paciente = new Paciente();
        $paciente->deletePaciente($id);
        header("Location:".BASE_URL."paciente");

    }

    public function insert(){

        $paciente = new Paciente();
        $dados['sexos'] = $paciente->getAllSexos();

        $this->loadTemplate('paciente/cadastro', $dados);

    }

    public function post(){

        $paciente['rua']       = $_POST['rua'];
        $paciente['peso']       = $_POST['peso'];
        $paciente['numero']    = $_POST['numero'];
        $paciente['cep']       = $_POST['cep'];
        $paciente['nome']      = $_POST['nome'];
        $paciente['data_nasc'] = $_POST['data_nasc'];
        $paciente['rg']        = $_POST['rg'];
        $paciente['cpf']       = $_POST['cpf'];
        $paciente['sexo_id']   = $_POST['sexo_id'];
        $paciente['telefoneprincipal']  = $_POST['telefoneprincipal'];
        $paciente['telefonesecundario'] = $_POST['telefonesecundario'];
        $paciente['convenios']          = $_POST['convenios'];
        $paciente['paciente_antigo_especial'] = $_POST['paciente_antigo_especial'];
        $paciente['tipo_indicacao']           = $_POST['tipo_indicacao'];
        $paciente['altura']                   = $_POST['altura'];

        $pa = new Paciente();
        $pa->insertEndereco($paciente);

        header("Location:".BASE_URL."paciente");

    }

    public function anamnese($id){

        $p = new Paciente();

        $data = $p->anamneseExists($id);

        if(empty($data)){
           $dados['anamneseExists'] = false;
        }else{
            $dados['anamnese'] = $data;
            $dados['anamneseExists'] = true;
        }

        $dados['anamnese']['paciente_id'] = $id;



        $dados['id'] = $id;

        $this->loadTemplate('paciente/anamnese', $dados);
    }

    public function createAnamnese(){

        $anamnese = [];

        $anamnese['atividade']   = $_POST['atividade'];
        $anamnese['medicamento'] = $_POST['medicamento'];
        $anamnese['queixa']     = $_POST['queixa'];
        $anamnese['fratura']    = $_POST['fratura'];
        $anamnese['cirurgia']    = $_POST['cirurgia'];
        $anamnese['paciente_id'] = $_POST['paciente_id'];

        $p = new Paciente();
        $p->createAnamnese($anamnese);

        header("Location:".BASE_URL."paciente");
    }

    public function updateAnamnese(){

        $anamnese = [];

        $anamnese['atividade']   = $_POST['atividade'];
        $anamnese['medicamento'] = $_POST['medicamento'];
        $anamnese['queixa']     = $_POST['queixa'];
        $anamnese['fratura']    = $_POST['fratura'];
        $anamnese['cirurgia']    = $_POST['cirurgia'];
        $anamnese['paciente_id'] = $_POST['paciente_id'];
        $anamnese['anamnese_id'] = $_POST['anamnese_id'];

        $p = new Paciente();
        $p->updateAnamnese($anamnese);

        header("Location:".BASE_URL."paciente");

    }



}