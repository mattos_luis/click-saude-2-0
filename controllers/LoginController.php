<?php

class LoginController extends Controller {

    public function index(){

        $this->authUser();

    }

    public function authUser(){

       $this->loadTemplate('login/login');

    }

    public function doLogin(){

        $user['username'] = $_POST['username'];
        $user['senha']    = $_POST['senha'];

        $auth = new Auth();
        $dados = $auth->doLogin($user);

        if($_SESSION['isLogged'] == true){
            header("Location:".BASE_URL);
        }else{
            header("Location:".BASE_URL);
        }

    }

    public function logout(){
       unset($_SESSION['username']);
       unset($_SESSION['senha']);

       $_SESSION['isLogged'] = false;

       header("Location:".BASE_URL);
    }




}