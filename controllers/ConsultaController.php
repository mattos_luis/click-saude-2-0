<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 12/1/2017
 * Time: 9:40 PM
 */

class ConsultaController extends Controller
{
    public function __construct()
    {
        $this->canAccess();
    }

    public function index(){

        if(!isset($_POST['filtro_realizada'])){
            $filtro['filtro_realizada'] = 0;
        }else{
            $filtro['filtro_realizada'] = intval($_POST['filtro_realizada']);
        }


        $this->getAll($filtro);
    }

    public function getAll($filtro){

        $c = new Consulta();
        $dados['consultas'] = $c->getAll($filtro);

        $this->loadTemplate('consulta/consulta', $dados);
    }

    public function marcarConsulta(){

        $c = new Consulta();
        $dados['pacientes'] = $c->getPacientes();
        $this->loadTemplate('consulta/marcar-consulta', $dados);
    }

    public function post(){

        $consulta['data']         = $_POST['data_consulta'];
        $consulta['hora']         = $_POST['horario'];
        $consulta['paciente']     = intval($_POST['paciente_id']);
        $consulta['profissional'] = 1;

        $c = new Consulta();
        $c->marcarConsulta($consulta);

        header("Location:".BASE_URL."consulta");

    }

    public function realizarConsulta($id){

        $dados['consulta'] = array('id' => $id);

        $this->loadTemplate('consulta/realizar-consulta', $dados);

    }

    public function postConsulta(){

        $consulta['anotacao'] = $_POST['anotacao'];
        $consulta['foto']     = $_POST['foto'];
        $consulta['video']    = $_POST['video'];
        $consulta['id_consulta']       = $_POST['id_consulta'];

        $c = new Consulta();
        $c->realizarConsulta($consulta);

        header("Location:".BASE_URL."consulta");

    }

    public function remarcarConsulta($id){

        $c = new Consulta();
        $dados['consulta'] = $c->getConsultaById($id);

        $this->loadTemplate('consulta/edit', $dados);
    }

    public function updateConsulta(){

        $consulta['data_consulta'] = $_POST['data_consulta'];
        $consulta['horario']       = $_POST['horario'];
        $consulta['id']            = $_POST['id'];

        $c = new Consulta();

        $c->reagendarConsulta($consulta);

        header("Location:".BASE_URL."consulta");

    }
}