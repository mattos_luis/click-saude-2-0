<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 12/17/2017
 * Time: 12:07 AM
 */

class RelatorioController extends Controller
{

    public function __construct()
    {
        $this->canAccess();
    }

    public function index()
    {
        $this->getRelatorioSexo();
    }

    public function getRelatorioSexo(){

        $this->loadTemplate('relatorio/relatorio');

    }

    public function dados(){
        $relatorio = new Relatorio();
        $dados['relatorioSexo'] = $relatorio->pequisaSexo();


        header('Content-Type: application/json');

        print json_encode($dados['relatorioSexo']);
    }
}