<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 12/1/2017
 * Time: 9:33 PM
 */

class ProfissionalController extends Controller
{

    public function __construct()
    {
        $this->canAccess();
    }

    public function index(){
        $this->getAll();
    }


    public function getAll(){

        $profissional = new Profissional();
        $dados['profissionais'] = $profissional->getAllProfissionais();

        $this->loadTemplate('profissional/profissional',$dados);

    }

    public function get($id){

        //Chamar model método getPacienteById;

    }

    public function edit($id){

        if($this->isAdmin()){

            $profissional = new Profissional();
            $dados['profissional'] = $profissional->getProfissionalById($id);
            $dados['sexos'] = $profissional->getAllSexos();

            $this->loadTemplate('profissional/alterar', $dados);

        }

    }

    public function update(){

        if($this->isAdmin()){

            $profissional['rua']       = $_POST['rua'];
            $profissional['peso']       = $_POST['peso'];
            $profissional['numero']    = $_POST['numero'];
            $profissional['cep']       = $_POST['cep'];
            $profissional['nome']      = $_POST['nome'];
            $profissional['data_nasc'] = $_POST['data_nasc'];
            $profissional['rg']        = $_POST['rg'];
            $profissional['cpf']       = $_POST['cpf'];
            $profissional['sexo_id']   = $_POST['sexo_id'];
            $profissional['telefoneprincipal']  = $_POST['telefoneprincipal'];
            $profissional['telefonesecundario'] = $_POST['telefonesecundario'];
            $profissional['tipo']               = $_POST['tipo'];
            $profissional['id_pessoa']          = $_POST['id_pessoa'];
            $profissional['id_profissional']    = $_POST['id_profissional'];
            $profissional['id_endereco']        = $_POST['id_endereco'];

            $pr = new Profissional();
            $pr->updateEndereco($profissional);

            header("Location:".BASE_URL."profissional");

        }


    }

    public function delete($id){

       if($this->isAdmin()){

           $profissional = new Profissional();
           $profissional->deleteProfissional($id);
           header("Location:".BASE_URL."profissional");

       }

    }

    public function insert(){

        $profissional = new Profissional();
        $dados['sexos'] = $profissional->getAllSexos();

        $this->loadTemplate('profissional/cadastro', $dados);

    }

    public function post(){

        if($this->isAdmin()){

            $profissional['rua']       = $_POST['rua'];
            $profissional['peso']       = $_POST['peso'];
            $profissional['numero']    = $_POST['numero'];
            $profissional['cep']       = $_POST['cep'];
            $profissional['nome']      = $_POST['nome'];
            $profissional['data_nasc'] = $_POST['data_nasc'];
            $profissional['rg']        = $_POST['rg'];
            $profissional['cpf']       = $_POST['cpf'];
            $profissional['sexo_id']   = $_POST['sexo_id'];
            $profissional['telefoneprincipal']  = $_POST['telefoneprincipal'];
            $profissional['telefonesecundario'] = $_POST['telefonesecundario'];
            $profissional['tipo']               = $_POST['tipo'];


            $pr = new Profissional();
            $pr->insertEndereco($profissional);

            header("Location:".BASE_URL."profissional");

        }

    }

    public function isAdmin(){

        if(isset($_SESSION['perfil']) && $_SESSION['perfil'] == 'admin'){
            return true;
        }else{
            header("Location:".BASE_URL."profissional");
        }

    }
}