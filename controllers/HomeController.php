<?php

class HomeController extends Controller {

    public function __construct()
    {
        $this->canAccess();
    }

    public function index(){

        $utils = new Utils();
        $dados['totalPacientes'] = $utils->getCountPacientes();
        $dados['totalProfissionais'] = $utils->getCountProfissionais();
        $dados['totalConsultas'] = $utils->getCountConsultas();



        return $this->loadTemplate('index', $dados);
    }

}