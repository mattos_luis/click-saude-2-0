<?php

require 'environment.php';



$config = array();
if(ENVIRONMENT == 'development') {
	define("BASE_URL", "http://localhost/click-saude-2-0/");
	$config['dbname'] = 'click_saude';
	$config['host'] = 'localhost';
	$config['dbuser'] = 'postgres';
	$config['dbpass'] = '1234';
} else {
	define("BASE_URL", "http://localhost/click-saude-2-0/");
	$config['dbname'] = 'click_saude';
	$config['host'] = 'localhost';
	$config['dbuser'] = 'postgres';
	$config['dbpass'] = '1234';
}

global $db;
try {
	$db = new PDO("pgsql:dbname=".$config['dbname'].";host=".$config['host'], $config['dbuser'], $config['dbpass']);
} catch(PDOException $e) {
	echo "ERRO: ".$e->getMessage();
	exit;
}