<div class="container">

    <div class="row" style="margin-top: 7%">
        <div class="col s6">
            <h3 class="lighter">O que você deseja fazer?</h3>
        </div>
    </div>

    <div class="row">
        <div class="col s4 ">
            <div class="card card-dark-green">
                <a class="white-text" href="<?=BASE_URL?>paciente">
                    <div class="card-content white-text">
                        <h4 class="center">
                            <i class="fa fa-wheelchair material-icons" aria-hidden="true"></i>
                            Paciente
                        </h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="col s4 ">
            <div class="card card-light-green">
                <a class="white-text" href="<?=BASE_URL?>consulta">
                    <div class="card-content white-text">
                        <h4 class="center">
                            <i class="fa fa-id-card-o material-icons" aria-hidden="true"></i>
                            Marcar consulta
                        </h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="col s4">
            <div class="card card-dark-green">
                <a class="white-text" href="<?=BASE_URL?>profissional">
                    <div class="card-content white-text">
                        <h4 class="center">
                            <i class="fa fa-handshake-o" aria-hidden="true"></i>
                            Profissional
                        </h4>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row" >
        <div class="col s12">
            <h3 class="lighter">Monitoramento (últimas 24 horas)</h3>
        </div>
    </div>

    <div class="row">
        <div class="col s4">
            <div class="card card-dark-red" >
                <div class="card-content white-text">
                    <h4 class="center">
                        <?= $totalPacientes['totpacientes']; ?> Novos Pacientes
                    </h4>
                </div>
            </div>
        </div>

        <div class="col s4">
            <div class="card card-dark-red">
                <div class="card-content white-text">
                    <h4 class="center">
                        <?= $totalProfissionais['totprofissionais']; ?> Novos Profissionais
                    </h4>
                </div>
            </div>
        </div>

        <div class="col s4">
            <div class="card card-dark-red">
                <div class="card-content white-text">
                    <h4 class="center">
                        <?= $totalConsultas['totconsultas']; ?> Consultas Feitas
                    </h4>
                </div   >
            </div>
        </div>
    </div>
</div>

</div>


