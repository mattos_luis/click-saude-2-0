<div class="container">
    <h3 class="lighter" style="margin-top: 4%;">Gerenciar Consultas</h3>

   <div class="row">
       <div class="input-field col s6">
          <button class="btn waves-light waves-effect modal-trigger" data-target="modal1">FILTRAR CONSULTAS</button>
       </div>
   </div>

    <ul class="collapsible popout" data-collapsible="accordion">

        <?php foreach($consultas as $c):?>

            <li>
                <div class="collapsible-header">
                       <i class="fa fa-calendar material-icons" aria-hidden="true"></i><?=$c['nome']?>
                </div>

                <div class="collapsible-body">
                    <div class="row">
                        <?php if($c['realizada'] == 0): ?>
                            <div class="col s6 center">
                                <a href="<?= BASE_URL ?>consulta/realizarConsulta/<?= $c['id']?>" class="waves-effect btn-flat waves-light">
                                    <i class="fa fa-file-text left material-icons"></i>
                                    Realizar Consulta
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if($c['realizada'] == 0): ?>
                            <div class="col s6 center">
                                <a href="<?= BASE_URL ?>consulta/remarcarConsulta/<?= $c['id']?>" class="waves-effect btn-flat waves-light">
                                    <i class="fa fa-edit left material-icons"></i>
                                    Reagendar
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if($c['realizada'] == 1): ?>
                            <div class="col s12 center">
                                <a href="<?= BASE_URL ?>paciente/anamnese" class="waves-effect btn-flat waves-light">
                                    <i class="fa fa-edit left material-icons" aria-hidden="true"></i>
                                    Gerar atestado
                                </a>
                            </div>
                        <?php endif; ?>

                    </div>

                    <?php if($c['realizada'] == 0): ?>

                        <div class="row">
                            <h4 class=" center lighter">Data da consulta: <?=$c['data_consulta']?> ás <?=$c['horario']?></h4>
                        </div>

                    <?php endif; ?>

                    <?php if($c['realizada'] == 1): ?>
                        <div class="row">
                           <div class="col s6 offset-s3">
                               <h4 class="center lighter">Anotações da consulta</h4>
                               <p style="font-size:16px;">
                                   <?= $c['anotacao']; ?>
                               </p>
                           </div>
                        </div>
                        <div class="row">
                            <p style="font-size:16px;" class="center">Consulta realizada em: <?=$c['data_consulta']?> ás <?=$c['horario']?></p>
                        </div>
                    <?php endif; ?>
                </div>
        <?php endforeach;?>
    </ul>
</div>


<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
        <h4>Filtrar Consultas</h4>
        <form class="col s6" action="<?= BASE_URL ?>consulta" method="post" style="margin:10% 0;">
            <div class="input-field col s12">
                <select name="filtro_realizada" >
                    <option disabled selected>Escolha uma opção</option>
                    <option value="1">Realizadas</option>
                    <option value="0">Não realizadas</option>
                </select>
            </div>
    </div>
            <div class="modal-footer">
                <button type="submit" class="modal-action modal-close waves-effect waves-green btn-flat">Filtrar</button>
            </div>
        </form>
</div>

<div class="fixed-action-btn vertical click-to-toggle">
    <a class="btn-floating btn-large teal lighten-1">
        <i class="material-icons">mode_edit</i>
    </a>
    <ul>
        <li><a href="<?= BASE_URL ?>relatorio/getRelatorioSexo" class="btn-floating tooltipped teal lighten-1" data-position="left" data-delay="50" data-tooltip="Relatório de prontuários"><i class="material-icons">insert_chart</i></a></li>
        <li><a href="<?= BASE_URL ?>consulta/marcarConsulta" class="btn-floating tooltipped teal lighten-1" data-position="left" data-delay="50" data-tooltip="Marcar nova Consulta"><i class="material-icons">add</i></a></li>
    </ul>
</div>








