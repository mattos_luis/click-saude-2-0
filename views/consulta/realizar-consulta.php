<div class="container" style="margin-top:3%;">
    <div class="row">
        <div class="card col s8 offset-s2">
           <div class="card-content">
               <div class="row">
                   <div class="col s10 offset-s1 ">
                       <h3 class="center lighter ">Relatório de Consulta</h3>

                       <form action="<?= BASE_URL ?>consulta/postConsulta"  method="post" style="margin-top: 40px !important;">

                           <input type="hidden" name="id_consulta" value="<?= $consulta['id']; ?> ">

                           <div class="input-field col s12">
                               <i class="material-icons prefix">mode_edit</i>
                               <textarea name="anotacao" id="icon_prefix2" class="materialize-textarea"></textarea>
                               <label for="icon_prefix2">Anotações sobre a consulta</label>
                           </div>

                           <div class="file-field col s12 input-field">
                               <div class="btn">
                                   <span>Anexar Foto</span>
                                   <input name="foto" type="file">
                               </div>
                               <div class="file-path-wrapper">
                                   <input class="file-path validate" placeholder="Sua foto aqui" type="text">
                               </div>
                           </div>

                           <div class="file-field col s12 input-field">
                               <div class="btn">
                                   <span>Anexar Vídeo</span>
                                   <input type="file">
                               </div>
                               <div class="file-path-wrapper">
                                   <input name="video" class="file-path validate" placeholder="Seu video aqui" type="text">
                               </div>
                           </div>
                   </div>
               </div>

               <div class="row">
                   <button type="submit" class="waves-effect waves-light btn col s4 offset-s4">FINALIZAR CONSULTA</button>
               </div>
               </form>
           </div>
        </div>
    </div>
</div>