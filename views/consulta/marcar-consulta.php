<div class="container" style="margin-top:3%;">
    <div class="row">
        <div class="card col s6 offset-s3">
            <div class="card-content">
                <div class="row">
                    <div class="col s10 offset-s1 ">
                        <h3 class="center lighter ">Marcar nova consulta</h3>

                        <form action="<?= BASE_URL ?>consulta/post"  method="post" style="margin-top: 40px !important;">

                            <div class="input-field col s12">
                                <input class="input-field datepicker" type="datetime" name="data_consulta" placeholder="Data da Consulta">
                            </div>

                            <div class="input-field col s12">
                                <input class="input-field timepicker" type="time" name="horario" placeholder="Horário da Consulta">
                            </div>

                            <div class="input-field col s12">
                                <select name="paciente_id">
                                    <?php foreach($pacientes as $paciente): ?>
                                        <option value="<?= $paciente['id'] ?>"><?= $paciente['nome'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label>Paciente</label>
                            </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col s12 ">
                        <button type="submit" class="col s6 offset-s3 waves-light waves-effect btn">Marcar Consulta</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="fixed-action-btn">
    <a href="<?= BASE_URL ?>consulta" class="btn-floating btn-large teal lighten-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Voltar para consultas">
        <i class="large material-icons">arrow_back</i>
    </a>
</div>