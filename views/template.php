<html>
	<head>
		<title>Click Saúde</title>
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/main.css" />

        <meta charset="UTF-8">
        <title></title>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/materialize.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/main.css"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body>



    <?php if($_SESSION['isLogged'] == true): ?>
        <nav>
            <div class="nav-wrapper">
                <a href="<?=BASE_URL; ?>" class="brand-logo" style="font-family: "Roboto">
                <i class="fa fa-heartbeat material-icons" aria-hidden="true"></i>
                Click Saúde <span style="font-size:20px;" class="right hide-on-med-and-down" > - revitalizando sua vida!</span>
                </a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul  class="right hide-on-med-and-down">
                    <li>
                        <a href="<?=BASE_URL; ?>paciente" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para realizar o cadastro">
                            <i class="fa fa-wheelchair material-icons left" aria-hidden="true"></i>
                            Pacientes
                        </a>
                    </li>

                    <li>
                        <a href="<?=BASE_URL; ?>profissional" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para realizar o cadastro">
                            <i class="fa fa-handshake-o material-icons left" aria-hidden="true"></i>
                            Profissionais
                        </a>
                    </li>

                    <li>
                        <a href="<?=BASE_URL; ?>consulta" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para marcar uma consulta">
                            <i class="fa fa-id-card-o material-icons left" aria-hidden="true"></i>
                            Consultas
                        </a>
                    </li>

                    <li>
                        <a href="<?=BASE_URL?>login/logout" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para sair do sistema">
                            <i class="fa fa-sign-out material-icons left" aria-hidden="true"></i>
                            Sair do Sistema
                        </a>
                    </li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li>
                        <a href="<?=BASE_URL; ?>paciente" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para realizar o cadastro">
                            <i class="fa fa-wheelchair material-icons left" aria-hidden="true"></i>
                            Pacientes
                        </a>
                    </li>

                    <li>
                        <a href="<?=BASE_URL; ?>profissional" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para realizar o cadastro">
                            <i class="fa fa-handshake-o material-icons left" aria-hidden="true"></i>
                            Profissionais
                        </a>
                    </li>

                    <li>
                        <a href="<?=BASE_URL; ?>consulta" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para marcar uma consulta">
                            <i class="fa fa-id-card-o material-icons left" aria-hidden="true"></i>
                            Consultas
                        </a>
                    </li>

                    <li>
                        <a href="<?=BASE_URL?>login/logout" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para sair do sistema">
                            <i class="fa fa-sign-out material-icons left" aria-hidden="true"></i>
                            Sair do Sistema
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    <?php endif; ?>

		<?php $this->loadViewInTemplate($viewName, $viewData); ?>


        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/materialize.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/inicializarJquery.js"></script>

    </body>
</html>