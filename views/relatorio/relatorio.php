<html>
<head>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

</head>

<div class="row" style="margin-top: 10%;">
    <div class="col s5 offset-s3 center">
        <div id="chart-container">
            <canvas id="myChart"></canvas>
        </div>
    </div>
</div>
