<div class="container container-paciente">
    <div class="row">
        <div class="col s6">
            <h3 style="padding-left:10px;" class="lighter">Paciente: <?= $paciente['nome']; ?></h3>
        </div>
    </div>
    <form action="<?= BASE_URL ?>paciente/update" method="post">
    <input type="hidden" name="id_pessoa" value="<?= $paciente['id_pessoa'] ?>" >
    <input type="hidden" name="id_endereco" value="<?= $paciente['id_endereco']?>">
    <input type="hidden" name="id_paciente" value="<?= $paciente['id_paciente'] ?>" >

    <div class="row">
            <div class="col s6">
                <div class="input-field col s12">
                    <input type="text" autofocus required name="nome" value="<?= $paciente['nome']; ?>">
                    <label for="nome">Nome</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" class="datepicker" required name="data_nasc" value="<?= $paciente['data_nasc']; ?>">
                    <label for="data_nasc">Data de Nascimento</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="rg" required value="<?= $paciente['rg']; ?>">
                    <label for="RG">RG</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="cpf" required value="<?= $paciente['cpf']; ?>">
                    <label for="cpf">CPF</label>
                </div>

                <div class="input-field col s12">
                    <select name="sexo_id">
                        <?php foreach($sexos as $sexo): ?>
                            <option  <?= $sexo['id'] == $paciente['sexo_id'] ? 'selected': '' ?> value="<?= $sexo['id'] ?>"><?= $sexo['descricao'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <label>Sexo</label>
                </div>


                <div class="input-field col s12">
                    <input type="text"  name="rua" required value="<?= $paciente['rua']; ?>">
                    <label for="cpf">Rua</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="numero" required value="<?= $paciente['numero']; ?>">
                    <label for="cpf">Numero</label>
                </div>
            </div>

            <div class="col s6">
                <div class="input-field col s12">
                    <input type="text" name="cep" required value="<?= $paciente['cep']; ?>">
                    <label for="cep">CEP</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="telefoneprincipal" required value="<?= $paciente['telefoneprincipal']; ?>">
                    <label for="telefoneprincipal">Telefone Principal</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="telefonesecundario" required value="<?= $paciente['telefonesecundario']; ?>">
                    <label for="telefonesecundario">Telefone Secundario</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="convenios" required value="<?= $paciente['convenios']; ?>">
                    <label for="telefonesecundario">Convenios </label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="tipo_indicacao" required value="<?= $paciente['tipo_indicacao']; ?>">
                    <label for="telefonesecundario">Tipo de indicação</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="altura" required value="<?= $paciente['altura']; ?>">
                    <label for="telefonesecundario">Altura</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="peso" required value="<?= $paciente['peso']; ?>">
                    <label for="peso">Peso</label>
                </div>

                <div class="col s12">
                    <p>
                        <input type="checkbox" id="test5"  name="paciente_antigo_especial" <?= $paciente['paciente_antigo_especial'] == true ? 'checked'  : ''; ?>>
                        <label for="test5">Paciente antigo especial</label>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12" style="margin-top: 20px;">
                <button type="submit" class="col s4 offset-s4 waves-effect waves-light blue btn teal lighten-2" >Atualizar Paciente</button>
            </div>
        </div>
</div>
</form>