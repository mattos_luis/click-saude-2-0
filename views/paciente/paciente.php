<div class="container">
    <h3 class="lighter">Gerenciar Pacientes</h3>
    <ul class="collapsible popout" data-collapsible="accordion">
        <?php foreach($pacientes as $p): ?>
            <li>
                <div class="collapsible-header">

                        <i class="fa fa-user material-icons" aria-hidden="true"></i><?= $p['nome'] ?>

                </div>

                <div class="collapsible-body">
                    <div class="row">
                        <div class="col s4 center">
                            <a href="<?= BASE_URL ?>paciente/edit/<?= $p['id'] ?>" class="waves-effect btn-flat waves-light">
                                <i class="material-icons left fa fa-edit"></i>
                                Editar
                            </a>
                        </div>

                        <div class="col s4 center">
                            <a href="<?= BASE_URL ?>paciente/delete/<?= $p['id'] ?>" class="waves-effect btn-flat waves-light">
                                <i class="material-icons left fa fa-trash"></i>
                                Excluir
                            </a>
                        </div>

                        <div class="col s4 center">
                            <a href="<?= BASE_URL ?>paciente/anamnese/<?= $p['id'] ?>" class="waves-effect btn-flat waves-light">
                                <i class="fa fa-list-alt left material-icons" aria-hidden="true"></i>
                                Anamnese
                            </a>
                        </div>
                    </div>

                    <div class="row center">
                        <div class="col s4">
                            <b>Nome</b><br/>
                            <?= $p['nome'] ?>
                        </div>

                        <div class="col s4">
                            <b>CPF</b><br/>
                            <?= $p['cpf'] ?>
                        </div>

                        <div class="col s4">
                            <b>RG</b><br/>
                            <?= $p['rg'] ?>
                        </div>
                    </div>

                    <div class="row center">
                        <div class="col s4">
                            <b>Telefone 1</b><br/>
                            <?= $p['telefoneprincipal'] ?>
                        </div>

                        <div class="col s4">
                            <b>Telefone 2</b><br/>
                            <?= $p['telefonesecundario'] ?>
                        </div>

                        <div class="col s4">
                            <b>Possui convênio?</b><br/>
                            <?= $p['convenios'] ?>
                        </div>
                    </div>

                    <div class="row center">
                        <div class="col s4">
                            <b>Paciente Especial?</b><br/>
                            <?= $p['paciente_antigo_especial'] == true ? 'Sim': 'Não'; ?>
                        </div>

                        <div class="col s4">
                            <b>Tipo indicação</b><br/>
                            <?= $p['tipo_indicacao'] ?>
                        </div>

                        <div class="col s4">
                            <b>Data de Nascimento</b><br/>
                            <?= $p['data_nasc'] ?>
                        </div>
                    </div>

                    <div class="row center">
                        <div class="col s4">
                            <b>Altura</b><br/>
                            <?= $p['altura'] ?>
                        </div>

                        <div class="col s4">
                            <b>Peso</b><br/>
                            <?= $p['peso'] ?>
                        </div>

                        <div class="col s4">
                            <b>CEP</b><br/>
                            <?= $p['cep'] ?>
                        </div>
                    </div>

                    <div class="row center">
                        <div class="col s6 offset-s3">
                            <b>Rua </b><?= $p['rua'] ?> , <b>nº</b> <?= $p['numero'] ?>
                        </div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>


<div class="fixed-action-btn">
    <a href="<?= BASE_URL ?>paciente/insert" class="btn-floating btn-large teal lighten-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Cadastrar novo Paciente">
        <i class="large material-icons">add</i>
    </a>
</div>








