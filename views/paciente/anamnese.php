<div class="container" style="margin-top:3%;">
    <div class="row">
        <div class="card col s8 offset-s2">
            <div class="card-content">
                <div class="row">
                    <div class="col s10 offset-s1 ">
                        <h3 class="center lighter ">Anamnese</h3>

                        <form action="<?= BASE_URL ?>paciente/<?= $anamneseExists == false ? 'createAnamnese':'updateAnamnese'; ?>" method="post">

                            <input type="hidden" name="paciente_id" value="<?= isset($anamnese['paciente_id']) ? $anamnese['paciente_id'] : null;  ?>">
                            <input type="hidden" name="anamnese_id" value="<?= isset($anamnese['id']) ? $anamnese['id'] : null; ?>">

                            <div class="row">
                                <div class="col s12">
                                    <div class="input-field col s12">
                                        <label for="nome">Pratica Atividade Física</label>
                                        <input type="text" autofocus required name="atividade" value="<?= isset($anamnese['atividade_fisica']) ? $anamnese['atividade_fisica'] : null; ?>">
                                    </div>

                                    <div class="input-field col s12">
                                        <label for="nome">Medicamentos utilizados</label>
                                        <input type="text" autofocus required name="medicamento" value="<?= isset($anamnese['medicamentos_em_uso']) ? $anamnese['medicamentos_em_uso'] : null; ?>">
                                    </div>

                                    <div class="input-field col s12">
                                        <label for="nome">Cirurgias realizadas</label>
                                        <input type="text" autofocus required name="cirurgia" value="<?= isset($anamnese['queixas']) ? $anamnese['queixas'] : null; ?>">
                                    </div>

                                    <div class="input-field col s12">
                                        <label for="nome">Histórico de Fraturas</label>
                                        <input type="text" autofocus required name="fratura" value="<?= isset($anamnese['fraturas']) ? $anamnese['fraturas'] : null; ?>">
                                    </div>

                                    <div class="input-field col s12">
                                        <label for="nome">Queixas</label>
                                        <input type="text" autofocus required name="queixa" value="<?= isset($anamnese['cirurgia']) ? $anamnese['cirurgia'] : null; ?>">
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12" >
                                    <button type="submit" class="col s4 offset-s4 waves-effect waves-light blue btn teal lighten-2" ><?= $anamneseExists == false ? 'Cadastrar':'Atualizar'; ?></button>
                                </div>
                            </div>
                        </form>

            </div>
        </div>
    </div>
</div>