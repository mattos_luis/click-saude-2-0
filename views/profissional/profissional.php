<div class="container">
    <h3 class="lighter">Gerenciar Profissionais</h3>
    <ul class="collapsible popout" data-collapsible="accordion">
        <?php foreach($profissionais as $p): ?>
        <li>
            <div class="collapsible-header">

                <i class="fa fa-user material-icons" aria-hidden="true"></i><?= $p['nome'] ?>

            </div>
                <div class="collapsible-body">
                    <?php if(isset($_SESSION['perfil']) && $_SESSION['perfil'] == 'admin'): ?>
                        <div class="row">
                            <div class="col l4 offset-l4">
                                <a href="<?= BASE_URL ?>profissional/edit/<?= $p['id'] ?>"  class="waves-effect btn-flat waves-light">
                                    <i class="material-icons left fa fa-edit"></i>
                                    Editar
                                </a>


                                <a href="<?= BASE_URL ?>profissional/delete/<?= $p['id'] ?>" class="waves-effect btn-flat waves-light">
                                    <i class="material-icons left fa fa-trash"></i>
                                    Excluir
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="row center">
                        <div class="col s4">
                            <b>Nome</b><br/>
                            <?= $p['nome'] ?>
                        </div>

                        <div class="col s4">
                            <b>CPF</b><br/>
                            <?= $p['cpf'] ?>
                        </div>

                        <div class="col s4">
                            <b>RG</b><br/>
                            <?= $p['rg'] ?>
                        </div>
                    </div>

                    <div class="row center">
                        <div class="col s4">
                            <b>Telefone 1</b><br/>
                            <?= $p['telefoneprincipal'] ?>
                        </div>

                        <div class="col s4">
                            <b>Telefone 2</b><br/>
                            <?= $p['telefonesecundario'] ?>
                        </div>

                        <div class="col s4">
                            <b>Data de Nascimento</b><br/>
                            <?= $p['data_nasc'] ?>
                        </div>
                    </div>

                    <div class="row center">

                        <div class="col s4">
                            <b>CEP</b><br/>
                            <?= $p['cep'] ?>
                        </div>
                    </div>
                    <div class="row center">
                        <div class="col s6 offset-s3">
                            <b>Rua </b><?= $p['rua'] ?> , <b>nº</b> <?= $p['numero'] ?>
                        </div>
                    </div>
                </div>
        </li>
        <?php endforeach; ?>
    </ul>
</div>

<div class="fixed-action-btn">
    <a href="<?= BASE_URL ?>/profissional/insert" class="btn-floating btn-large teal lighten-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Cadastrar novo Profissional">
        <i class="large material-icons">add</i>
    </a>
</div>








