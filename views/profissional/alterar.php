<div class="container container-paciente">
    <div class="row">
        <div class="col s6">
            <h3 style="padding-left:10px;" class="lighter">Profissional: <?= $profissional['nome']; ?></h3>
        </div>
    </div>
    <form action="<?= BASE_URL ?>profissional/update" method="post">
    <input type="hidden" name="id_pessoa" value="<?= $profissional['id_pessoa'] ?>" >
    <input type="hidden" name="id_endereco" value="<?= $profissional['id_endereco']?>">
    <input type="hidden" name="id_profissional" value="<?= $profissional['id_profissional'] ?>" >

    <div class="row">
            <div class="col s6">
                <div class="input-field col s12">
                    <input type="text" autofocus required name="nome" value="<?= $profissional['nome']; ?>">
                    <label for="nome">Nome</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" class="datepicker" required name="data_nasc" value="<?= $profissional['data_nasc']; ?>">
                    <label for="data_nasc">Data de Nascimento</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="rg" required value="<?= $profissional['rg']; ?>">
                    <label for="RG">RG</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="cpf" required value="<?= $profissional['cpf']; ?>">
                    <label for="cpf">CPF</label>
                </div>

                <div class="input-field col s12">
                    <select name="sexo_id">
                        <?php foreach($sexos as $sexo): ?>
                            <option  <?= $sexo['id'] == $profissional['sexo_id'] ? 'selected': '' ?> value="<?= $sexo['id'] ?>"><?= $sexo['descricao'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <label>Sexo</label>
                </div>

            </div>

            <div class="col s6">

                <div class="input-field col s12">
                    <input type="text"  name="rua" required value="<?= $profissional['rua']; ?>">
                    <label for="cpf">Rua</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="numero" required value="<?= $profissional['numero']; ?>">
                    <label for="cpf">Numero</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="cep" required value="<?= $profissional['cep']; ?>">
                    <label for="cep">CEP</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="telefoneprincipal" required value="<?= $profissional['telefoneprincipal']; ?>">
                    <label for="telefoneprincipal">Telefone Principal</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="telefonesecundario" required value="<?= $profissional['telefonesecundario']; ?>">
                    <label for="telefonesecundario">Telefone Secundario</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="tipo" required value="<?= $profissional['tipo']; ?>">
                    <label for="tipo">Tipo de Profissional</label>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col s12" >
                <button type="submit" class="col s4 offset-s4 waves-effect waves-light blue btn teal lighten-2" >Inserir Profissional</button>
            </div>
        </div>
    </form>
</div>
