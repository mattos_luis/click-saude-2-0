<div class="container container-profissional">
    <div class="row">
        <div class="col s6">
            <h3 style="padding-left:10px;" class="lighter">Cadastro de Profissional</h3>
        </div>
    </div>
    <form action="<?= BASE_URL ?>profissional/post" method="post">


        <div class="row">
            <div class="col s6">
                <div class="input-field col s12">
                    <input type="text" autofocus required name="nome" value="">
                    <label for="nome">Nome</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" class="datepicker" required name="data_nasc" value="">
                    <label for="data_nasc">Data de Nascimento</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="rg" required value="">
                    <label for="RG">RG</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="cpf" required value="">
                    <label for="cpf">CPF</label>
                </div>


                <div class="input-field col s12">
                    <input type="password" name="senha" required value="">
                    <label for="senha">Crie sua Senha</label>
                </div>


                <div class="input-field col s12">
                    <select name="sexo_id">
                      <?php foreach($sexos as $sexo): ?>
                          <option value="<?= $sexo['id'] ?>"><?= $sexo['descricao'] ?></option>
                      <?php endforeach; ?>
                    </select>
                    <label>Sexo</label>
                </div>

            </div>

            <div class="col s6">


                <div class="input-field col s12">
                    <input type="text"  name="rua" required value="">
                    <label for="cpf">Rua</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="numero" required value="">
                    <label for="cpf">Numero</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="cep" required value="">
                    <label for="cep">CEP</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="telefoneprincipal" required value="">
                    <label for="telefoneprincipal">Telefone Principal</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="telefonesecundario" required value="">
                    <label for="telefonesecundario">Telefone Secundario</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="tipo" required value="">
                    <label for="tipo">Tipo</label>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col s12" >
                <button type="submit" class="col s4 offset-s4 waves-effect waves-light blue btn teal lighten-2" >Cadastrar Profissional</button>
            </div>
        </div>
    </form>
</div>

<div class="fixed-action-btn">
    <a href="<?= BASE_URL ?>profissional" class="btn-floating btn-large teal lighten-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Voltar para profissionais">
        <i class="large material-icons">arrow_back</i>
    </a>
</div>