$(document).ready(function() {
    $('select').material_select();

    $(function(){
        $(".button-collapse").sideNav();
    });

    $('.modal').modal();



    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
    });

    $('.carousel.carousel-slider').carousel({fullWidth: true});

    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
    });

    $('.tooltipped').tooltip({delay: 50});


    $.ajax({
        url: "http://localhost/click-saude-2-0/relatorio/dados",
        method: "GET",
        success: function(data) {

            var descricao = [];
            var quantidade = [];

            for(var i in data) {
                console.log(data[i].qtd);
                descricao.push(data[i].descricao);
                quantidade.push(data[i].qtd);
            }



            var chartdata = {
                labels: descricao,
                datasets : [
                    {
                        label: 'Relatório quantidade de pessoas por sexo',
                        backgroundColor:  ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        hoverBackgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
                        hoverBorderColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
                        data: quantidade
                    }
                ]
            };

            var ctx = $("#myChart");

            var barGraph = new Chart(ctx, {
                type: 'bar',
                data: chartdata,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                },
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

